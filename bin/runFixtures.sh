#!/usr/bin/env bash

php bin/console server:stop
php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console doctrine:fixtures:load
php bin/console cache:clear
php bin/console server:start *:8000
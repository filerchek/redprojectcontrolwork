<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="FavorRepository")
 */
class Favor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\JoinColumn(nullable=false)
     */
    private $redditdate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="favores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="ReddPost", inversedBy="favores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $reddPost;

    public function getId()
    {
        return $this->id;
    }

    public function getRedditdate(): ?\DateTimeInterface
    {
        return $this->redditdate;
    }

    public function setRedditdate(\DateTimeInterface $redditdate): self
    {
        $this->redditdate = $redditdate;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getReddPost(): ?ReddPost
    {
        return $this->reddPost;
    }

    public function setReddPost(?ReddPost $reddPost): self
    {
        $this->reddPost = $reddPost;

        return $this;
    }
}

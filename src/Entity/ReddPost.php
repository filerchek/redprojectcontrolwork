<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="RedditRepository")
 */
class ReddPost
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Favor", mappedBy="reddPost")
     */
    private $favores;

    public function __construct()
    {
        $this->favores = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Favor[]
     */
    public function getFavores(): Collection
    {
        return $this->favores;
    }

    public function addFavor(Favor $favorite): self
    {
        if (!$this->favores->contains($favorite)) {
            $this->favores[] = $favorite;
            $favorite->setReddPost($this);
        }

        return $this;
    }

    public function removeFavorite(Favor $favor): self
    {
        if ($this->favores->contains($favor)) {
            $this->favores->removeElement($favor);
            // set the owning side to null (unless already changed)
            if ($favor->getReddPost() === $this) {
                $favor->setReddPost(null);
            }
        }

        return $this;
    }

    public function count(){
        return count($this->favores);
    }
}

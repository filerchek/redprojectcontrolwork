<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Favor", mappedBy="user")
     */
    private $favores;

    public function __construct()
    {
        parent::__construct();
        $this->favores = new ArrayCollection();
    }

    /**
     * @return Collection|Favor[]
     */
    public function getFavores(): Collection
    {
        return $this->favores;
    }

    public function addFavorite(Favor $favor): self
    {
        if (!$this->favores->contains($favor)) {
            $this->favores[] = $favor;
            $favor->setUser($this);
        }

        return $this;
    }

    public function removeFavorite(Favor $favor): self
    {
        if ($this->favores->contains($favor)) {
            $this->favores->removeElement($favor);
            // set the owning side to null (unless already changed)
            if ($favor->getUser() === $this) {
                $favor->setUser(null);
            }
        }

        return $this;
    }

    public function count(){
        return count($this->favores);
    }
}
<?php

namespace App\Controller;

use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @Route("/", name="mainIndex")
     * @param Request $request
     * @param ApiContext $ContextApi
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mainIndex(Request $request, ApiContext $ContextApi)
    {
        $data = $request->request->get('searchForm');

        if ($data) {
            try {
                $redditData = $ContextApi->getBySearched($data);
            } catch (ApiException $e) {
                $error = $e->getMessage() . ' ||| ' . $e->getCode();
            }
        }

        return $this->render('index/index.html.twig', [
            'redditData' => $redditData['data']['children'] ?? null,
            'error' => $error ?? null
        ]);
    }

    /**
     * @Route("/show", name="show")
     * @param ApiContext $apiContext
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(ApiContext $apiContext, Request $request)
    {
        $data = $request->query->get('data');

        try {
            $redditData = $apiContext->getById(['id' => $data]);
        } catch (ApiException $e) {
            $error = $e->getMessage() . ' ||| ' . $e->getCode();
        }

        return $this->render('index/show.html.twig', [
            'redditData' => $redditData['data']['children'] ?? null,
            'error' => $error ?? null
        ]);
    }

    /**
     * @Route("/adToMyFavores{id}", name="adToMyFavores")
     * @param string $id
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adMyFavoresAction(string $id, ApiContext $apiContext)
    {


        try {
            $redditData = $apiContext->getById(['id' => $id]);
        } catch (ApiException $e) {
            $error = $e->getMessage() . ' ||| ' . $e->getCode();
        }

        return $this->render('index/show.html.twig', [
            'redditData' => $redditData['data']['children'] ?? null,
            'error' => $error ?? null
        ]);
    }

    /**
     * @Route("/myFavores", name="myFavores")
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myFavoresAction(ApiContext $apiContext)
    {
        $data = [];

        foreach ($data as $key => $item) {
            $data[$key] = $item->getName();
        }

        try {
            $redditData = $apiContext->getById(['id' => $data]);
        } catch (ApiException $e) {
            $error = $e->getMessage() . ' ||| ' . $e->getCode();
        }

        return $this->render('index/show.html.twig', [
            'redditData' => $redditData['data']['children'] ?? null,
            'error' => $error ?? null
        ]);
    }

    /**
     * @Route("/Community", name="Community")
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function communityAction(ApiContext $apiContext, ObjectManager $manager)
    {
        $data = [];

        foreach ($data as $key => $item) {
            $data[$key] = $item->getName();
        }

        try {
            $redditData = $apiContext->getById(['id' => $data]);
        } catch (ApiException $e) {
            $error = $e->getMessage() . ' ||| ' . $e->getCode();
        }

        return $this->render('index/show.html.twig', [
            'redditData' => $redditData['data']['children'] ?? null,
            'error' => $error ?? null
        ]);
    }
}

<?php

namespace App\Repository;

use App\Entity\ReddPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReddPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReddPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReddPost[]    findAll()
 * @method ReddPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RedditRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReddPost::class);
    }


}

<?php

namespace App\Repository;

use App\Entity\Favor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Favor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favor[]    findAll()
 * @method Favor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Favor::class);
    }
}

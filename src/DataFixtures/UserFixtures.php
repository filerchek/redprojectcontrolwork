<?php
namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     * @throws \App\Model\Api\ApiException
     */
    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => '123@123.ru',
            'username' => 'gendotar',
            'password' => 'pass_1234',
            'roles' => ['USER']
        ],false);

        $manager->persist($user);

        $user2 = $this->userHandler->createNewUser([
            'email' => 'ewq@qwe.ru',
            'username' => 'tourist',
            'password' => 'qwerty',
            'roles' => ['USER']
        ],false);

        $manager->persist($user2);

        $user3 = $this->userHandler->createNewUser([
            'email' => 'test@mail.ru',
            'username' => 'cappa',
            'password' => 'super',
            'roles' => ['USER']
        ],false);

        $manager->persist($user3);

        $manager->flush();
    }
}

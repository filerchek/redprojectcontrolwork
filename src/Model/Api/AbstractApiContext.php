<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 8:58 PM
 */

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AbstractApiContext
{

    /** @var Curl */
    protected $curl;

    /** @var ContainerInterface */
    protected $container;

    const METHOD_GET = 1;
    const METHOD_HEAD = 2;

    /**
     * AbstractApiContext constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        try {
            $this->curl = new Curl();
        } catch (\ErrorException $e) {
        }

        $this->curl->setJsonDecoder(function () {
            $args = func_get_args();

            $response = json_decode($args[0], true);
            if ($response === null) {
                $response = $args[0];
            }
            return $response;
        });
    }

    /**
     * @param string $url
     * @param int $Method
     * @param array $data
     * @return bool|null
     * @throws ApiException
     */
    protected function makeQuery(string $url, int $Method, array $data)
    {
        $data = array_merge($data, ['type' => 'link']);

        $this->curl->get($url);
        $this->curl->setHeader(
            'user-agent',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36 OPR/51.0.2830.55'
        );

        switch ($Method) {
            case self::METHOD_GET:
                $this->curl->get($url, $data);
                break;
            case self::METHOD_HEAD:
                $this->curl->head($url, $data);
                break;
            default:
                break;
        }

        if ($this->curl->error) {
            if ($Method == self::METHOD_HEAD && $this->curl->error == 404) {
                return false;
            }
            throw new ApiException(
                $this->curl->errorMessage,
                $this->curl->errorCode,
                null,
                $this->curl->response
            );
        } else {
            if ($Method == self::METHOD_HEAD) {
                return true;
            }
            return $this->curl->response;
        }
    }

    protected function getParameter(string $name)
    {
        return $this->container->getParameter($name);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\User;

use App\Entity\Landlord;
use App\Entity\Tenant;
use App\Entity\User;
use App\Model\Api\ApiContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(
        ContainerInterface $container, ApiContext $apiContext
    )
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws \App\Model\Api\ApiException
     */
    public function createNewUser(array $data, bool $encodePassword = true) {
        $user = new User();
        $this->createNewAbstractUser($user, $data, $encodePassword);

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws \App\Model\Api\ApiException
     */
    public function createNewAbstractUser(User $user, array $data, bool $encodePassword = true) {
        $user->setEmail($data['email']);
        $user->setUsername($data['username']);
        $user->setRoles($data['roles'] ?? ['ROLE_USER']);

        if($encodePassword) {
            $password = $this->encodePlainPassword($data['password']);
        } else {
            $password = $data['password'];
        }

        $user->setPassword($password);

        return $user;
    }



    /**
     * @param string $password
     * @return string
     * @throws \App\Model\Api\ApiException
     */
    public function encodePlainPassword(string $password): string
    {
        return $this->apiContext->encodePassword($password);
    }

    public function makeUserSession(User $user) {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }
}
